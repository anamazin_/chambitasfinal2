var mongoose = require('mongoose');
var Schema = mongoose.Schema;
mongoose.connect('mongodb://localhost:27017/chambitas');

var user = new Schema({

	first_name: {type: String, require:true},
	last_name: {type: String, require:true},
	email: {type: String, require:true},
	password: {type: String, require:true},
	type: {type: String, require:true},
	country: {type: String, require:true},
	city: {type: String, require:true},
	state: {type: String, require:true}

});
module.exports = mongoose.model('User', user);