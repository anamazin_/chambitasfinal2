var express = require('express');
var router = express.Router();
var path = require('path');
var models = path.join(__dirname, '../models');

/* GET home page. */
router.get('/index', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/', function(req, res, next) {

  var varUser = path.join(models, '/user');
  var User = require(varUser);

  res.render('views/home', data);

});

router.get("/homeUsuarios",function(req, res){
  if(!req.session.user){
    res.redirect('/views/home');
  }
  else{
    res.render('homeUsuarios', req.session.user);
  }
});

router.get("/homeWorkers",function(req, res){
  if(!req.session.user){
    res.redirect('/views/home');
  }
  else{
    res.render('homeWorkers', req.session.user);
  }
});

router.get("/chat",function(req, res){
  if(!req.session.user){
    res.redirect('/views/home');
  }
});


router.get("/home",function(req, res){
  res.render('home');
});

router.get("/changeProfile",function(req, res){
  res.render('editarPerfil');
  if(!req.session.user){
    res.redirect('/views/home');
  }

});

router.get("/iniciarSesion",function(req, res){
  res.render('iniciarSesion');
});
router.get("/guardados",function(req, res){
  res.render('guardados');
  if(!req.session.user){
    res.redirect('/views/home');
  }
});

router.get("/soy",function(req, res){
  res.render('soy');
});

router.get("/busco",function(req, res){
  res.render('busco');
});

router.get("/registro",function(req, res){
  res.render('registro');
});

module.exports = router;